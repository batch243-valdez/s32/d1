
const http = require("http");
const url = require("url");
const port = 4000;

const server = http.createServer((request, response) => {

// HTTP Routing Methods: Get, Post, Put and Delete
    if(request.url == "/items" && request.method == "GET"){

        // HTTP method of the incoming request can be accesed via the method property of the request parameter
        // The method "GET" means that we will be retrieving or reading an information
        response.writeHead(200,{"Content-Type": "text/plain"});
        response.end("Data retrieved from the database");
    }
    else if(request.url == "/items" && request.method == "POST"){
        response.writeHead(200,{"Content-Type": "text/plain"});
        response.end("Data to be sent to the Database")
    }
    else{
        response.writeHead(404,{"Content-Type": "text/plain"});
        response.end("Method not found!");
    }   
})

server.listen(port);

console.log("Server is running at localhost:4000");