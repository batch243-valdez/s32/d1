const http = require("http");
const url = require("url");
const port = 4000;

// Mock Database
    let directory = [
        {
            "name" : "Brandon",
            "email": "brandom@mail.com"
        },
        {
            "name:": "Jobert",
            "email": "jobert@mail.com"
        }
    ]

    const server = http.createServer((request, response) => {

        // Route for returning all items upon receiving a GET Request
        if(request.url == "/users" && request.method == "GET"){

            response.writeHead(200, {"Content-Type" : "application/json"});
            response.write(JSON.stringify(directory));
            response.end();

        }

        // add user to the database
        else if(request.url == "/users" && request.method == "POST"){

            // declare and initialize a "requestBody" variable to an empty string
            // this will act as a placeholder of 
            let requestBody = "";

            // A stream is a sequence of data
            // Data is received from the client is processed in the "data stream"
            // The information provided from the request object enters a sequence called "data" the code below will be triggered
            // Data step - this reads the "data stream" and processes it as the request body 

            request.on("data", (data) => {
            // Assigns the data retrieved from the from the data stream to the requestBody
            requestBody += data;
            });
            // response end step - only runs after the request has completely been sent
            request.on("end", () => {

                console.log(typeof requestBody);

                requestBody = JSON.parse(requestBody);

                // Create a new object representing the new mock database record
                let newUser = {
                    "name": requestBody.name,
                    "email": requestBody.email
                }

                // Add the new user into the mock database
                directory.push(newUser)
                console.log(directory);

                response.writeHead(200, {"Content-Type": "application/json"})
                response.write(JSON.stringify(newUser));
                response.end();
            });
        }

        else{
            response.end("Invalid Input!");
        }


    })

    server.listen(port);

    console.log("Server running at localhost:4000");